package br.com.mdr.jobexpress.controller;

import android.content.Context;

import java.util.List;

import br.com.mdr.jobexpress.model.Usuario;
import br.com.mdr.jobexpress.model.Vaga;

/**
 * Created by MarlonDRocha on 26/03/17.
 */

public class VagaController {
    public boolean salvaVaga(Vaga v) {
        try {
            if (v.getId() > 0)
                v.update();
            else
                v.insert();
            return true;
        }catch(Exception e) {
            return false;
        }
    }

    public boolean deletaVaga(Vaga v) {
        try {
            v.delete();
            return true;
        }catch(Exception e) {
            return false;
        }
    }

    public Vaga getVagaById(int id, Context context) {
        Vaga v = new Vaga(context);
        v.setId(id);
        v.getVaga();
        return v;
    }

    public List<Vaga> listaVagas(Context context) {
        try {
            Vaga v = new Vaga(context);
            return v.getVagas(context);
        }catch(Exception e) {
            return null;
        }
    }
    public List<Vaga> listaVagasById(Context context, int idUsuario) {
        try {
            Vaga v = new Vaga(context);
            return v.getVagasById(context, idUsuario);
        }catch(Exception e) {
            return null;
        }
    }
}
