package br.com.mdr.jobexpress.adapters;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.IntegerRes;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import br.com.mdr.jobexpress.Interfaces.RecyclerViewOnClickListenerHack;
import br.com.mdr.jobexpress.model.Usuario;
import br.com.mdr.jobexpress.model.Vaga;
import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.view.CadastroVagaActivity;
import br.com.mdr.jobexpress.view.MainActivity;

import static br.com.mdr.jobexpress.R.id.imgLogoEmpresa;


public class VagaAdapter extends RecyclerView.Adapter<VagaAdapter.MyViewHolder> {
    private Context mContext;
    private List<Vaga> mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;
    private Usuario usuario;


    public VagaAdapter(Context c, List<Vaga> l, Usuario u){
        usuario = u;
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = mContext.getResources().getDisplayMetrics().density;
        width = mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f);
        height = (width / 16) * 9;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_vacancy, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        //Pega as views do viewHolder de acordo com sua posição e insere as informações da vaga
        myViewHolder.txtDescricao.setText(mList.get(position).getDescricao());
        DecimalFormat formataMoeda = new DecimalFormat("##,###,###,##0.00",
                new DecimalFormatSymbols(new Locale("pt", "BR")));
        formataMoeda.setMinimumFractionDigits(2);
        formataMoeda.setParseBigDecimal(true);
        myViewHolder.txtSalario.setText("Salário: R$ " + formataMoeda.format(mList.get(position).getSalario()));
        if (usuario.isAdm())
            myViewHolder.btnCandidatar.setText("Editar Vaga");
        else
            myViewHolder.btnCandidatar.setText("Candidatar-se");

        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            myViewHolder.imgEmpresa.setImageResource(mList.get(position).getImg());
        }
        else{*/
        File imgFile = new  File(mList.get(position).getImg());
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            myViewHolder.imgEmpresa.setImageBitmap(myBitmap);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(myBitmap, 300, 300, true);
            myViewHolder.imgEmpresa.setImageBitmap(bitmapReduzido);
            myViewHolder.imgEmpresa.setScaleType(ImageView.ScaleType.FIT_XY);
            myViewHolder.imgEmpresa.setTag(myBitmap);
        }
        myViewHolder.imgEmpresa.setTag(String.valueOf(mList.get(position).getId()));
            //Bitmap bitmap = BitmapFactory.decodeResource( mContext.getResources(), mList.get(position).getImg());
            //bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

            //bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 4, width, height, false, false, true, true);
            //myViewHolder.imgEmpresa.setImageBitmap(bitmap);
        //}
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }


    public void addListItem(Vaga v, int position){
        mList.add(v);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imgEmpresa;
        public TextView txtDescricao;
        public TextView txtSalario;
        public Button btnCandidatar;

        public MyViewHolder(final View itemView) {
            super(itemView);

            imgEmpresa = (ImageView) itemView.findViewById(R.id.imgEmpresa);
            txtDescricao = (TextView) itemView.findViewById(R.id.txtDescricao);
            txtSalario = (TextView) itemView.findViewById(R.id.txtSalario);
            btnCandidatar = (Button) itemView.findViewById(R.id.btnCandidata);

            btnCandidatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (usuario.isAdm()) {
                        Intent i = new Intent(mContext, CadastroVagaActivity.class);
                        i.putExtra("id_vaga", Integer.parseInt(imgEmpresa.getTag().toString()));
                        mContext.startActivity(i);
                    }else {
                        Toast.makeText(mContext, "Você se candidatou à vaga: " +
                                txtDescricao.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(mContext, txtDescricao.getText().toString(), Toast.LENGTH_SHORT).show();
                    //int img = mList.get(getPosition()).getImg();
                    //criaNotificacao(txtDescricao.getText().toString(), txtSalario.getText().toString(),
                            //mList.get(getPosition()).getImg());
                }
            });
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }

    private void criaNotificacao(String vaga, String salario, int imgEmpresa) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        Intent i = new Intent(mContext, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pIntent);

        //Texto do ticket
        builder.setTicker("Candidatura Enviada!");
        builder.setSmallIcon(R.drawable.ic_stat_custom);
        builder.setAutoCancel(true);
        Notification n = builder.build();
        //Infla o xml da notificação como um RemoteView
        RemoteViews remoteView = new RemoteViews(mContext.getPackageName(), R.layout.notification);
        final String msgVaga = "Parabéns! Você se candidatou à vaga " + vaga;
        final String msgSalario = salario;
        remoteView.setTextViewText(R.id.txtNotificationVaga, msgVaga);
        remoteView.setTextViewText(R.id.txtNotificationSalario, msgSalario);
        n.contentView = remoteView;
        //Verifica a versão do Android e mostra a notificação expandida
        //caso seja API 16+
        if (Build.VERSION.SDK_INT >= 16) {
            RemoteViews expandedView = new RemoteViews(mContext.getPackageName(), R.layout.notification_expanded);
            expandedView.setTextViewText(R.id.txtParabens, "Parabéns Marlon!");
            expandedView.setTextViewText(R.id.txtVoce, "Você se candidatou à vaga abaixo:");
            expandedView.setTextViewText(R.id.txtDescricaoVaga, vaga);
            expandedView.setTextViewText(R.id.txtDescricaoSalario, msgSalario);
            expandedView.setImageViewResource(R.id.imgEmpresa, imgEmpresa);

            n.bigContentView = expandedView;
        }
        NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, n);
    }
}
