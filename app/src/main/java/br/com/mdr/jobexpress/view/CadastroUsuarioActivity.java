package br.com.mdr.jobexpress.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import br.com.mdr.jobexpress.controller.UsuarioController;
import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.model.Usuario;

public class CadastroUsuarioActivity extends AppCompatActivity {
    Button btnCadastrar;
    EditText edtEmail, edtSenha;
    RadioButton rbAdm;

    UsuarioController controller = new UsuarioController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
        iniciaComponentes();
    }

    private void iniciaComponentes() {
        btnCadastrar = (Button) findViewById(R.id.btnCadastraUsuario);
        edtEmail = (EditText) findViewById(R.id.edtEmailCad);
        edtSenha = (EditText) findViewById(R.id.edtSenhaCad);
        rbAdm = (RadioButton) findViewById(R.id.rbAdm);
    }

    public void cadastraUsuario(View v) {
        if (!edtEmail.getText().toString().equals("") && !edtSenha.getText().toString().equals("")) {
            Usuario u = new Usuario(getApplicationContext());
            u.setEmail(edtEmail.getText().toString());
            u.setSenha(edtSenha.getText().toString());
            u.setAdm(rbAdm.isChecked());
            controller.salvaUsuario(u);
            Toast.makeText(getApplicationContext(), "Usuário Cadastrado!", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            Toast.makeText(getApplicationContext(), "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
        }
    }
}
