package br.com.mdr.jobexpress.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.Calendar;

import br.com.mdr.jobexpress.BuildConfig;
import br.com.mdr.jobexpress.controller.UsuarioController;
import br.com.mdr.jobexpress.controller.VagaController;
import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.extras.ImageFilePath;
import br.com.mdr.jobexpress.model.Usuario;
import br.com.mdr.jobexpress.model.Vaga;

import static android.R.attr.id;

public class CadastroVagaActivity extends AppCompatActivity {
    private Bitmap bitmap;
    EditText edtDescVaga, edtSalario, edtEmpresa;
    ImageView imgLogoEmpresa, imgCamera;
    Button btnCadastrar, btnDeletar;
    private String dirImg;
    private Usuario usuario;
    private Vaga vaga;
    private VagaController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_vaga);
        iniciaComponentes();

        Intent intent = getIntent();
        UsuarioController usuarioController = new UsuarioController();
        int idUsuario = intent.getIntExtra("id_usuario", 0);
        usuario = usuarioController.getUsuarioById(idUsuario, getApplicationContext());

        if (controller == null)
            controller = new VagaController();
        int id = intent.getIntExtra("id_vaga", 0);
        if (id > 0) {
            vaga = controller.getVagaById(id, getApplicationContext());
            edtDescVaga.setText(vaga.getDescricao());
            edtSalario.setText(String.valueOf(vaga.getSalario()));
            edtEmpresa.setText(vaga.getEmpesa());
            dirImg = vaga.getImg();
            getImage();
            btnDeletar.setVisibility(View.VISIBLE);
        }

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastraVaga();
            }
        });

        btnDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controller == null)
                    controller = new VagaController();
                controller.deletaVaga(vaga);
                Toast.makeText(getApplicationContext(), "Vaga deletada!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carregarGaleria();
            }
        });
    }

    public void carregarGaleria(){
        /*Este bloco pegaria uma imagem do sd, mas está com problemas para API 18+
            //Abre a biblioteca de imagens do Android
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, 0);
        * */

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        dirImg = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
        File arquivoFoto = new File(dirImg);

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                FileProvider.getUriForFile(getApplicationContext(),
                        BuildConfig.APPLICATION_ID + ".provider", arquivoFoto));

        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == CadastroVagaActivity.RESULT_OK) {
            getImage();
            /*Uri uri = data.getData();
            dirImg = data.getDataString();
            dirImg = data.getData().toString();
            dirImg = uri.getEncodedPath();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                imgLogoEmpresa.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        } else {
            Toast.makeText(this, "Aconteceu algo de errado.", Toast.LENGTH_SHORT).show();
        }
    }

    public void getImage(){
        if (dirImg != null){
            Bitmap bitmap = BitmapFactory.decodeFile(dirImg);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            imgLogoEmpresa.setImageBitmap(bitmapReduzido);
            imgLogoEmpresa.setScaleType(ImageView.ScaleType.FIT_XY);
            imgLogoEmpresa.setTag(dirImg);
        }
    }

    private boolean cadastraVaga() {
        try {
            VagaController controller = new VagaController();
            if (vaga == null)
                vaga = new Vaga(getApplicationContext());
            vaga.setIdUsuario(usuario.getId());
            vaga.setDescricao(edtDescVaga.getText().toString());
            vaga.setEmpresa(edtEmpresa.getText().toString());
            vaga.setSalario(Double.parseDouble(edtSalario.getText().toString()));
            vaga.setImg(dirImg);
            controller.salvaVaga(vaga);
            finish();
        } catch(Exception e) {
            Toast.makeText(this, "Vaga não cadastrada. Tente novamente!", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private void iniciaComponentes() {
        edtDescVaga = (EditText) findViewById(R.id.edtDescVaga);
        edtSalario = (EditText) findViewById(R.id.edtSalario);
        //edtSalario.addTextChangedListener(new MonetaryMask(edtSalario));
        edtEmpresa = (EditText) findViewById(R.id.edtEmpresa);
        imgLogoEmpresa = (ImageView) findViewById(R.id.imgLogoEmpresa);
        //carrega a imagem padrão (Logo da Empresa)
        imgLogoEmpresa.setImageResource(R.mipmap.vacancy_icon);
        imgCamera = (ImageView) findViewById(R.id.imgTirarFoto);
        btnCadastrar = (Button) findViewById(R.id.btnCadastraVaga);
        btnDeletar = (Button) findViewById(R.id.btnDeletaVaga);
        btnDeletar.setVisibility(View.INVISIBLE);
    }

    //Classe responsável por formatar a edittext com máscara de dinheiro
    private class MonetaryMask implements TextWatcher {
        private boolean mUpdating;
        private EditText mEditText;
        private NumberFormat mNF = NumberFormat.getCurrencyInstance();

        public MonetaryMask(EditText et) {
            mEditText = et;
        }

        @Override
        public void onTextChanged(CharSequence cs, int start, int before, int after) {
            if (this.mUpdating) {
                this.mUpdating = false;
                return;
            }

            this.mUpdating = true;
            String formattedValue = cs.toString().replace("$", "");

            try {
                double value = (Double.parseDouble(formattedValue));
                if (!formattedValue.contains(".")) {
                    value = value / 100;
                    formattedValue = this.mNF.format(value);
                }
            } catch (Exception e) {
                e.printStackTrace();
                formattedValue = this.mNF.format(0.0D);
            }

            this.mEditText.setText(formattedValue);
            this.mEditText.setSelection(formattedValue.length());
        }

        @Override
        public void beforeTextChanged(CharSequence cs, int start, int count, int after) {
            // Não iremos utilizar...
        }

        @Override
        public void afterTextChanged(Editable e) {
            // Não iremos utilizar...
        }

    }
}

