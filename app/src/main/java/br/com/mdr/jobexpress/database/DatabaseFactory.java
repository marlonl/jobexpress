package br.com.mdr.jobexpress.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by MarlonDRocha on 23/04/17.
 */

public class DatabaseFactory extends SQLiteOpenHelper {
    private static final String NOME_BANCO = "banco.db";
    private static final String TB_USUARIO = "usuario";
    private static final String TB_VAGA = "vaga";
    private static final int VERSAO = 1;

    public DatabaseFactory(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlUsuario = "CREATE TABLE "+ TB_USUARIO +
                " (id INTEGER PRIMARY KEY," +
                " email TEXT NOT NULL," +
                " senha TEXT NOT NULL," +
                " adm BOLLEAN NOT NULL);";
        db.execSQL(sqlUsuario);
        String sqlVaga = "CREATE TABLE "+ TB_VAGA +
                " (id INTEGER PRIMARY KEY," +
                " id_usuario INTEGER NOT NULL,"+
                " descricao TEXT NOT NULL," +
                " salario REAL NOT NULL," +
                " img TEXT NOT NULL," +
                " empresa TEXT NOT NULL);";
        db.execSQL(sqlVaga);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TB_USUARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TB_VAGA);
        onCreate(db);
    }
}
