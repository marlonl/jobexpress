package br.com.mdr.jobexpress.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.mdr.jobexpress.database.DatabaseFactory;

/**
 * Created by MarlonDRocha on 26/03/17.
 */

public class Vaga extends DatabaseFactory{
    private int id;
    private int idUsuario;
    private String descricao;
    private double salario;
    private String img;
    private String empresa;

    public Vaga(Context context) {
        super(context);
    }

    public void insert(){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = new ContentValues();
        dados.put("id_usuario", idUsuario);
        dados.put("descricao", descricao);
        dados.put("salario", salario);
        dados.put("img", img);
        dados.put("empresa", empresa);
        db.insert("vaga", null, dados);
    }

    public void update(){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = new ContentValues();
        dados.put("descricao", descricao);
        dados.put("salario", salario);
        dados.put("img", img);
        dados.put("empresa", empresa);

        String[] params ={String.valueOf(id)};
        db.update("vaga", dados, "id = ?", params);
    }

    public void delete(){
        SQLiteDatabase db = getWritableDatabase();

        String [] params = {String.valueOf(id)};
        db.delete("vaga", "id = ?", params);
    }

    public Vaga getVaga() {
        String sql = "select * from vaga where id = ?";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(this.id)});
        if (c.getCount() > 0) {
            c.moveToFirst();
            this.id = c.getInt(c.getColumnIndex("id"));
            this.descricao = c.getString(c.getColumnIndex("descricao"));
            this.salario = c.getDouble(c.getColumnIndex("salario"));
            this.img = c.getString(c.getColumnIndex("img"));
            this.empresa = c.getString(c.getColumnIndex("empresa"));
            c.close();
            return this;
        }else {
            return null;
        }
    }

    public List<Vaga> getVagas(Context context)  {
        String sql = "SELECT * FROM vaga;";
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sql, null);

        List<Vaga> vagas = new ArrayList<Vaga>();
        while (c.moveToNext()) {
            Vaga vaga = new Vaga(context);
            vaga.setId(c.getInt(c.getColumnIndex("id")));
            vaga.setDescricao(c.getString(c.getColumnIndex("descricao")));
            vaga.setSalario(c.getDouble(c.getColumnIndex("salario")));
            vaga.setImg(c.getString(c.getColumnIndex("img")));
            vaga.setEmpresa(c.getString(c.getColumnIndex("empresa")));
            vagas.add(vaga);
        }
        c.close();

        return vagas;
    }

    public List<Vaga> getVagasById(Context context, int pIdUsuario)  {
        String sql = "SELECT * FROM vaga WHERE id_usuario = ?;";
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(pIdUsuario)});

        List<Vaga> vagas = new ArrayList<Vaga>();
        while (c.moveToNext()) {
            Vaga vaga = new Vaga(context);
            vaga.setId(c.getInt(c.getColumnIndex("id")));
            vaga.setDescricao(c.getString(c.getColumnIndex("descricao")));
            vaga.setSalario(c.getDouble(c.getColumnIndex("salario")));
            vaga.setImg(c.getString(c.getColumnIndex("img")));
            vaga.setEmpresa(c.getString(c.getColumnIndex("empresa")));
            vagas.add(vaga);
        }
        c.close();

        return vagas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario(){ return idUsuario; }

    public void setIdUsuario(int idUsuario){ this.idUsuario = idUsuario; }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getEmpesa() {
        return this.empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
