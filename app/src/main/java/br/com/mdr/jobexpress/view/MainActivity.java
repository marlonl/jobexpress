package br.com.mdr.jobexpress.view;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import br.com.mdr.jobexpress.controller.UsuarioController;
import br.com.mdr.jobexpress.controller.VagaController;
import br.com.mdr.jobexpress.fragments.VagaFragment;
import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.model.Usuario;
import br.com.mdr.jobexpress.model.Vaga;

public class MainActivity extends AppCompatActivity {
    Usuario usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VagaFragment frag = (VagaFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(frag == null) {
            frag = new VagaFragment();
            Intent intent = getIntent();
            UsuarioController controller = new UsuarioController();
            int id = intent.getIntExtra("id_usuario", 0);
            usuario = controller.getUsuarioById(id, getApplicationContext());

            frag.mUsuario = usuario;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, frag);
            ft.commit();
        }
    }

    //Método responsável por criar os objetos das vagas, que serão utilizados no
    public List<Vaga> setListaVagas() {
        VagaController controller = new VagaController();
        //Verifica se usuário é administrador. Se sim lista as vagas cadastradas por ele apenas.
        List<Vaga> listAux = usuario.isAdm() ? controller.listaVagasById(getApplicationContext(), usuario.getId()) :
                controller.listaVagas(getApplicationContext());

        return listAux;
    }


}
