package br.com.mdr.jobexpress.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.mdr.jobexpress.adapters.VagaAdapter;
import br.com.mdr.jobexpress.Interfaces.RecyclerViewOnClickListenerHack;
import br.com.mdr.jobexpress.model.Usuario;
import br.com.mdr.jobexpress.model.Vaga;
import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.view.CadastroVagaActivity;
import br.com.mdr.jobexpress.view.MainActivity;

/**
 * Created by MarlonDRocha on 26/03/17.
 */

public class VagaFragment extends Fragment implements RecyclerViewOnClickListenerHack{
    private RecyclerView mRecyclerView;
    private FloatingActionButton fButton;
    private List<Vaga> mList;
    public Usuario mUsuario;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vacancy, container, false);
        fButton = (FloatingActionButton) view.findViewById(R.id.fButton);
        if (!mUsuario.isAdm())
            fButton.setVisibility(View.INVISIBLE);
        else
            fButton.setVisibility(View.VISIBLE);
        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), CadastroVagaActivity.class);
                i.putExtra("id_usuario", mUsuario.getId());
                startActivity(i);
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) mRecyclerView.getLayoutManager();

                VagaAdapter adapter = (VagaAdapter) mRecyclerView.getAdapter();

                if (mList.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    List<Vaga> listAux = ((MainActivity) getActivity()).setListaVagas();

                    for (int i = 0; i < listAux.size(); i++) {
                        adapter.addListItem(listAux.get(i), mList.size());
                    }
                }
            }
        });
        //listaVagas();
        return view;
    }

    @Override
    public void onResume() {
        listaVagas();
        super.onResume();
    }

    private void listaVagas() {
        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener( getActivity(), mRecyclerView, this ));

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(llm);

        mList = ((MainActivity) getActivity()).setListaVagas();
        VagaAdapter adapter = new VagaAdapter(getActivity(), mList, mUsuario);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onClickListener(View view, int position) {
        //Evento de click no CardView
        //Toast.makeText(getActivity(), "onClickListener(): "+position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        //Evento de click longo no CardView
        //Toast.makeText(getActivity(), "onLongClickListener(): "+position, Toast.LENGTH_SHORT).show();
    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerHack rvoclh){
            mContext = c;
            mRecyclerViewOnClickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onLongPressClickListener(cv,
                                rv.getChildPosition(cv) );
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onClickListener(cv,
                                rv.getChildPosition(cv) );
                    }
                    return(true);
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {}
    }
}
