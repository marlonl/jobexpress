package br.com.mdr.jobexpress.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.mdr.jobexpress.R;
import br.com.mdr.jobexpress.controller.UsuarioController;
import br.com.mdr.jobexpress.model.Usuario;

public class LoginActivity extends AppCompatActivity {
    private static String PREF_NAME = "UserPreferences";
    private static String USER_TYPE = "USER_TYPE";
    private static String USER_EMAIL = "USER_EMAIL";
    private static String USER_PASSWORD = "USER_PASSWORD";
    private static String SAVE_PASSWORD = "SAVE_PASSWORD";
    private boolean savePassword;
    private String userEmail, userPassword;
    private Usuario usuario;
    private boolean usePreferences = false;

    Button btnLogin;
    EditText edtEmail, edtSenha;
    CheckBox checkPassword;
    TextView txtCadastraUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        iniciaComponentes();
        getPreferences();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                efetuaLogin();
            }
        });
    }
    private void iniciaComponentes() {
        btnLogin = (Button) findViewById(R.id.btnSignInUser);
        edtEmail = (EditText) findViewById(R.id.edtEmailLogin);
        edtSenha = (EditText) findViewById(R.id.edtSenhaLogin);
        checkPassword = (CheckBox) findViewById(R.id.checkBox);
        //checkPassword.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey)));
        txtCadastraUsuario = (TextView) findViewById(R.id.txtCadastre);
    }

    public void abreTelaCadastro(View v) {
        Intent i = new Intent(getApplicationContext(), CadastroUsuarioActivity.class);
        startActivity(i);
    }

    private void efetuaLogin() {
        if (usuario == null || usuario.getId() == 0) {
            UsuarioController controller = new UsuarioController();
            usuario = controller.logaUsuario(getApplicationContext(), edtEmail.getText().toString(),
                    edtSenha.getText().toString());
        }
        if (usuario != null) {
            //Salva as informações de login
            if (!usePreferences)
                setPreferences();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.putExtra("id_usuario", usuario.getId());
            startActivity(i);
            usuario.setId(0);
        }else {
            Toast.makeText(getApplicationContext(), "Usuário ou senha inválido!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getPreferences() {
        SharedPreferences pref = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        savePassword = pref.getBoolean(SAVE_PASSWORD, false);
        if (savePassword) {
            usePreferences = true;
            userEmail = pref.getString(USER_EMAIL, "");
            userPassword = pref.getString(USER_PASSWORD, "");
            edtEmail.setText(userEmail);
            edtSenha.setText(userPassword);
            if (!userEmail.equals("") && !userPassword.equals("")) {
                efetuaLogin();
            }
        }
    }

    private void setPreferences() {
        SharedPreferences pref = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(SAVE_PASSWORD, checkPassword.isChecked());
        if (checkPassword.isChecked()) {
            editor.putString(USER_EMAIL, edtEmail.getText().toString());
            editor.putString(USER_PASSWORD, edtSenha.getText().toString());
        }
        //editor.clear();
        editor.apply();
    }
}
