package br.com.mdr.jobexpress.controller;

import android.content.Context;

import java.util.List;

import br.com.mdr.jobexpress.model.Usuario;

/**
 * Created by MarlonDRocha on 23/04/17.
 */

public class UsuarioController {
    public boolean salvaUsuario(Usuario u) {
        try {
            u.insert();
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    public Usuario getUsuarioById(int id, Context context) {
        Usuario u = new Usuario(context);
        u.setId(id);
        u.getUsuario();
        return u;
    }

    public boolean deletaUsuario(Usuario u) {
        try {
            u.delete();
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    public boolean atualizaUsuario(Usuario u) {
        try {
            u.update();
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    public Usuario logaUsuario(Context context, String email, String senha) {
        Usuario u = new Usuario(context);
        u.setEmail(email);
        u.setSenha(senha);
        return u.validaUsuario();
    }
}
