package br.com.mdr.jobexpress.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.com.mdr.jobexpress.database.DatabaseFactory;

/**
 * Created by MarlonDRocha on 23/04/17.
 */

public class Usuario extends DatabaseFactory{
    private int id;
    private String email;
    private String senha;
    private boolean adm;

    public Usuario(Context context) {
        super(context);
    }

    public void insert(){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = new ContentValues();
        dados.put("email", email);
        dados.put("senha", senha);
        dados.put("adm", adm);
        db.insert("usuario", null, dados);
    }

    public void update(){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = new ContentValues();
        dados.put("email", email);
        dados.put("senha", senha);
        dados.put("adm", adm);

        String[] params ={String.valueOf(id)};
        db.update("usuario", dados, "id = ?", params);
    }

    public void delete(){
        SQLiteDatabase db = getWritableDatabase();

        String [] params = {String.valueOf(id)};
        db.delete("usuario", "id = ?", params);
    }

    public Usuario getUsuario() {
        String sql = "select * from usuario where id = ?";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(this.id)});
        if (c.getCount() > 0) {
            c.moveToFirst();
            this.id = c.getInt(c.getColumnIndex("id"));
            this.email = c.getString(c.getColumnIndex("email"));
            this.senha = c.getString(c.getColumnIndex("senha"));
            this.adm = c.getInt(c.getColumnIndex("adm")) > 0;
            c.close();
            return this;
        }else {
            return null;
        }
    }

    public Usuario validaUsuario() {
        String sql = "select * from usuario where email = ? and senha = ?";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, new String[]{this.email,this.senha});
        if (c.getCount() > 0) {
            c.moveToFirst();
            this.id = c.getInt(c.getColumnIndex("id"));
            this.senha = c.getString(c.getColumnIndex("senha"));
            this.adm = c.getInt(c.getColumnIndex("adm")) > 0;
            c.close();
            return this;
        }else {
            return null;
        }
    }

    /*public List<Usuario> listaUsuarios()  {
        String sql = "SELECT * FROM usuario;";
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sql, null);

        List<Usuario> usuarios = new ArrayList<Usuario>();
        while (c.moveToNext()) {
            Usuario usuario = new Usuario(context);
            usuario.setId(c.getInt(c.getColumnIndex("id")));
            usuario.setEmail(c.getString(c.getColumnIndex("email")));
            usuario.setSenha(c.getString(c.getColumnIndex("senha")));
            usuario.setAdm(c.getInt(c.getColumnIndex("adm")) >0);
            usuarios.add(usuario);
        }
        c.close();

        return usuarios;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdm() {
        return adm;
    }

    public void setAdm(boolean adm) {
        this.adm = adm;
    }
}
